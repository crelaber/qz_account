<?php
/**
 * Created by PhpStorm.
 * User: Auser
 * Date: 2017/5/18
 * Time: 11:08
 */

namespace Api\Model;
use Think\Model;

class WechatEventReplyMsgModel extends Model
{

    public function getReplyContent($appid,$scene_id){
        $where =array(
            'appid'=>$appid,
            'scene_id'=>$scene_id
        );
        $data = $this->field('content,type')->where($where)->find();
        return $data;
    }


}