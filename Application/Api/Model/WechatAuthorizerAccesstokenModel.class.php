<?php
/**
 * Created by PhpStorm.
 * User: Auser
 * Date: 2017/5/8
 * Time: 11:08
 */
namespace Api\Model;
use Think\Model;
class WechatAuthorizerAccesstokenModel extends Model
{
    //储存token
    public function storeData($data)
    {
        $where = array(
            'authorizer_appid'=> $data['authorizer_appid'],
        );
        $select = $this->where($where)->find();
        if($select){
            $this->where($where)->delete();
        }
        $data['created_at']=date("Y-m-d H:i:s",time());
        $res = $this->data($data)->add();
        return $res;
    }


    //更新token
    public function updateData($authorizer_appid,$data){
        $where = array(
            'authorizer_appid'=>$authorizer_appid,
        );
        $data['updated_at']=date("Y-m-d H:i:s",time());
        $res = $this -> where($where)->save($data);
        return $res;
    }

    //根据appid获取刷新token
    public function getRefreshByAppid($appid){
        $where = array(
            'authorizer_appid'=>$appid
        );
        $res = $this->where($where)->find();
        return $res;
    }



}