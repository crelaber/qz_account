<?php

/**
 * Created by PhpStorm.
 * User: Auser
 * Date: 2017/5/8
 * Time: 10:16
 */
namespace Api\Model;
use Think\Model;
class WechatEventModel extends Model
{

    /*
     *根据事件类型获取所有事件
     */
    public function getDataByType($type){
        $where = array(
            'type'=>$type,
        );
        $data = $this->where($where)->select();
        return $data;
    }

    /**时间处理
     * @param $data
     */
    public function handleData($data){
        $type = $data['event_type'];
        switch($type){
            case "unauthorized":
                $this->deleteApp($data['authorizer_appid']);
                break;
            default:
                $this->saveApp($data);
        }
    }

    private function deleteApp($appid){
        $where = array(
            'authorizer_appid'=>$appid,
        );
        $this->where($where)->delete();
    }


    private function saveApp($data){
        $this->data($data)->add();
    }
}