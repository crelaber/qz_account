<?php
/**
 * Created by PhpStorm.
 * User: xi
 * Date: 16/1/12
 * Time: PM5:19
 */

namespace Api\Model;
use Think\Model;

class WechatBindInfoModel extends Model{


    public function getBindInfo($condition = array())
    {
        $data = $this->where($condition);

        $data = $data->find();

        return $data;
    }

    public function getByAppid($appid){
        $where['appid'] = $appid;
        return $this->getBindInfo($where);
    }

}