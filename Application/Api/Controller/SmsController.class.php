<?php
/**
 * Created by PhpStorm.
 * User: xi
 * Date: 16/1/20
 * Time: AM10:10
 */

namespace Api\Controller;


use Think\Controller;
class SmsController extends Controller {


    public function  sendSms(){

        $mobile = $_GET['mobile'];

        $code = rand(100000,999999);
        $data ="验证码：" . $code ;

        $account = C('SMS_ACCOUNT');
        $pwd = C('SMS_PWD');

        $post_data = array();
        $post_data['account'] = iconv('GB2312', 'GB2312',$account);
        $post_data['pswd'] = iconv('GB2312', 'GB2312',$pwd);
        $post_data['mobile'] =$mobile;
        $post_data['msg']=mb_convert_encoding("$data",'UTF-8', 'UTF-8');
        $post_data['needstatus']='false';
        $url='http://222.73.117.158/msg/HttpBatchSendSM?';
        $parse = parse_url($url);
        for($i=0;$i<10;$i++)

            $o="";
        foreach ($post_data as $k=>$v)
        {
            $o.= "$k=".urlencode($v)."&";
        }
        $post_data=substr($o,0,-1);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $result = curl_exec($ch);
        $execArray = array();
        $execArray['state'] = "-1";
        if($result){
            $resultArray = explode(",",$result);
            $execArray['state'] = $resultArray[1];
        }
        $execArray['code'] = $code;
        echo json_encode($execArray);
    }


}