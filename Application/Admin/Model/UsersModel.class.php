<?php
/**
 * Created by PhpStorm.
 * User: xi
 * Date: 16/1/12
 * Time: PM5:19
 */

namespace Admin\Model;
use Think\Model;

class UsersModel extends Model{


    public function getUser($condition = array())
    {
        $data = $this->where($condition);

        $data = $data->find();

        return $data;
    }

    public function getLatestUser($condition = array()){
        $order = "uid desc";
        $data = $this->where($condition)->order($order)->find();
        return $data;
    }

    public function create_name($user_name){
        $where = array();
        $where["user_name"] =  $user_name;
        $data = $this->where($where);
        $data = $data->find();
        if($data){
            $user_name = $user_name.rand(100,1000);
            $this->create_name($user_name);
        }else{
            return $user_name;
        }

    }

    public function check_username($user_name){
        $where = array();
        $where["user_name"] =  $user_name;
        $data = $this->where($where);
        $data = $data->find();
        if($data){
            return true;
        }
        return false;
    }

    public function getUserList($condition = array(),  $order = "uid desc", $p = 0, $num = 0, $limit = 0)
    {
        $list = $this->where($condition);

        if ($p && $num) {
            $list = $list->page($p . ',' . $num . '');
        }
        if ($limit) {
            $list = $list->limit($limit);
        }

        $list = $list->order($order)->select();

        return $list;
    }

    public function get_avatar($uid, $size = 'min', $return_type = 0)
    {
        $size = in_array($size, array(
            'max',
            'mid',
            'min',
            '50',
            '150',
            'big',
            'middle',
            'small'
        )) ? $size : 'real';

        $uid = abs(intval($uid));
        $uid = sprintf('%\'09d', $uid);
        $dir1 = substr($uid, 0, 3);
        $dir2 = substr($uid, 3, 2);
        $dir3 = substr($uid, 5, 2);

        if ($return_type == 1)
        {
            return $dir1 . '/' . $dir2 . '/' . $dir3 . '/';
        }

        if ($return_type == 2)
        {
            return substr($uid, -2) . '_avatar_' . $size;
        }

        return $dir1 . '/' . $dir2 . '/' . $dir3 . '/' . substr($uid, -2) . '_avatar_' . $size;
    }


 /*   //数据
    $data[] = array('id'=>1,'value'=>value1);
    $data[] = array('id'=>2,'value'=>value2);
    $data[] = array('id'=>3,'value'=>value3);
    $this->saveAll($data,表名);
 */
    public function saveAll($datas,$model){
        $model || $model=$this->name;
        $sql   = ''; //Sql
        $lists = []; //记录集$lists
        $pk    = $this->getPk();//获取主键
        foreach ($datas as $data) {
            foreach ($data as $key=>$value) {
                if($pk===$key){
                    $ids[]=$value;
                }else{
                    $lists[$key].= sprintf("WHEN %u THEN '%s' ",$data[$pk],$value);
                }
            }
        }
        foreach ($lists as $key => $value) {
            $sql.= sprintf("`%s` = CASE `%s` %s END,",$key,$pk,$value);
        }
        $sql = sprintf('UPDATE __%s__ SET %s WHERE %s IN ( %s )',strtoupper($model),rtrim($sql,','),$pk,implode(',',$ids));
        return M()->execute($sql);
    }



} 