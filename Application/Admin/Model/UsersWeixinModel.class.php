<?php
/**
 * Created by PhpStorm.
 * User: xi
 * Date: 16/1/12
 * Time: PM5:19
 */

namespace Admin\Model;
use Think\Model;

class UsersWeixinModel extends Model{


    public function getUser($condition = array())
    {
        $data = $this->where($condition);

        $data = $data->find();

        return $data;
    }


    public function getUserList($condition = array(),  $order = "uid desc", $p = 0, $num = 0, $limit = 0)
    {
        $list = $this->where($condition);

        if ($p && $num) {
            $list = $list->page($p . ',' . $num . '');
        }
        if ($limit) {
            $list = $list->limit($limit);
        }

        $list = $list->order($order)->select();

        return $list;
    }


} 