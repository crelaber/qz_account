<?php
/**
 * Created by PhpStorm.
 * User: xi
 * Date: 16/1/8
 * Time: PM5:42
 */
namespace Admin\Controller;
use Think\Controller;

class BaseController extends Controller{


    public function  _initialize(){

        if (ACTION_NAME == 'login' || ACTION_NAME == 'getVerify') {

        } else {
            if (!$this->is_login() || session("adminGroupId") == null) {
                $this->redirect('Admin/Login/login');
            }
        }
    }

    public function is_login()
    {
        if (session("adminName") && session("adminId")) {
            return true;
        } else {
            return false;
        }
    }
} 