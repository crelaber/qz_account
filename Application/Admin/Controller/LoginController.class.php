<?php
/**
 * Created by PhpStorm.
 * User: xi
 * Date: 16/1/8
 * Time: PM5:40
 */
namespace Admin\Controller;
use Admin\Controller\BaseController;
use Admin\Model;
class LoginController extends BaseController{


    public function login(){

        if (IS_POST) {
            $where = array();
            $where["user_name"] = I("post.username");
            $where["pwd"] = md5(I("post.pwd"));
            $user = D("Admin")->getUser($where);

            if ($user) {
                session("adminName", $user["user_name"]);
                session("adminId", $user["uid"]);
                session("adminGroupId", $user["group_id"]);
                $this->redirect("Admin/Index/index");
            } else {
                $this->redirect("Admin/Login/login");
            }
        }else{
            $this->display('./login');
        }

    }

    public function login_out(){

        session("adminName", null);
        session("adminId", null);
        session("adminGroupId", null);
        $this->display('./login');
    }

} 