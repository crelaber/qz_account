<?php
/**
 * Created by PhpStorm.
 * User: xi
 * Date: 16/1/11
 * Time: PM4:57
 */
namespace Admin\Controller;
use Admin\Controller\BaseController;
class IndexController extends BaseController{


    public function index(){

        $adminName = $_SESSION['adminName'];
        $this->assign('adminName', $adminName);
        $this->display('./index');
    }
}