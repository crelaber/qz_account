<?php
/**
 * Created by PhpStorm.
 * User: xi
 * Date: 16/1/12
 * Time: PM4:08
 */

namespace Admin\Controller;
use Api\Util\Helper;
use Admin\Model;

class WxComponentController extends  BaseController{


    public function authorizer(){
        $author = M('WechatEvent');
        $where = array();
        $authorizer_appid = I('post.userappid');
        if($authorizer_appid != ''){
            $where['authorizer_appid'] = array('like','%'.$authorizer_appid.'%');
        }
        $authorizer_event = I('post.event');
        if($authorizer_event != ''){
            $where['event_type'] = $authorizer_event;
        }
        $count = $author->where($where)->count();
        $p = getpage($count,15);
        $list = $author->field(true)->where($where)->order('creat_time desc')->limit($p->firstRow, $p->listRows)->select();
        $base_url = I("server.HTTP_HOST")."/Api/Terrace/authorizerInfo";
        $data = array();
        foreach($list as $k => $v){
            $data[$k]['id'] = $v['id'];
            $data[$k]['event_type'] = $v['event_type'];
            $data[$k]['appid'] = $v['authorizer_appid'];
            $data[$k]['decode'] = base64_encode($v['authorizer_appid']);
            $result_json =  Helper::curlRequest($base_url."?appid=".base64_encode($v['authorizer_appid']));
            $result = json_decode($result_json,true);
            $data[$k]['name']=$result['appAcountInfo']['nick_name'];
            $data[$k]['head']=$result['appAcountInfo']['head_img'];
            $data[$k]['zhuti']=$result['appAcountInfo']['principal_name'];
            $data[$k]['time']=$v['creat_time'];
        }

        $this->assign('authorizer_appid', $authorizer_appid);
        $this->assign('authorizer', $data);
        $this->assign('page', $p->show()); // 赋值分页输出
        $this->display("./authorizer");
    }


} 