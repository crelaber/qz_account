<?php
/**
 * Created by PhpStorm.
 * User: xi
 * Date: 16/1/12
 * Time: PM4:08
 */

namespace Admin\Controller;

use Admin\Model;
use Api\Util\Helper;
class UserController extends  BaseController{


    public function user_list_view(){



        $m = M('Users');
        $where = array();

        $username =  I('post.username');
        if($username != ''){
            $where['user_name'] = array('like','%'.$username.'%');
        }
        $mobile = I('post.mobile');
        if($mobile !=''){
            $where['mobile'] = array('like','%'.$mobile.'%');
        }

        $count = $m->where($where)->count();
        $p = getpage($count,15);
        $list = $m->field(true)->where($where)->order('reg_time desc')->limit($p->firstRow, $p->listRows)->select();
        foreach($list as $key=>$val) {
            // $headUrl = $val['headimgurl']/0;
            // $data[$key]['headimgurl'] = "<img src='$headUrl'/>";
            if ($val['sex'] == 1) {
                $list[$key]['sex'] = '男';
            } else if($val['sex'] == 2) {
                $list[$key]['sex'] = '女';
            }else{
                $list[$key]['sex'] = '保密';
            }

            $list[$key]['reg_time'] =  date("Y-m-d H:i:s", $val['reg_time']);
            if($val['from_platform'] == "component"){
                $appid = $val['from_appid'];
                $name = $this->getApp($appid);
                if($name){
                    $list[$key]['from_platform'] = $name;
                }else{
                    $list[$key]['from_platform'] = '问答平台';
                }
            }else if($val['from_platform'] == 0){
                $list[$key]['from_platform'] ='问答平台';
            }else if($val['from_platform'] == 1){
                $list[$key]['from_platform'] ='茄子营养师';
            }else if($val['from_platform'] == 2){
                $list[$key]['from_platform'] ='app平台';
            }else if($val['from_platform'] == 9){
                $list[$key]['from_platform'] ='NQ 大挑战';
            }
            if($val['from_type'] == 'weixin'){
                $list[$key]['from_type'] ='微信';
            }else if($val['from_type'] == 'mobile'){
                $list[$key]['from_type'] ='手机';
            }

        }
        $adminName = $_SESSION['adminName'];
        $this->assign('adminName', $adminName);
        $this->assign('username', $username);
        $this->assign('mobile', $mobile);
        $this->assign('list', $list); // 赋值数据集
        $this->assign('page', $p->show()); // 赋值分页输出
        $this->display("./user_list");
    }

    //根据appid获取授权公众号名称
    private function getApp($appid){
        $base_url = I("server.HTTP_HOST")."/Api/Terrace/authorizerInfo";
        $result_json =  Helper::curlRequest($base_url."?appid=".base64_encode($appid));
        $result = json_decode($result_json,true);
        if($result){
            return $result['appAcountInfo']['nick_name'];
        }else{
            return "";
        }
    }


    /*
     * 按月统计
     */
    public function statByMonth(){

        $m  = M('Users');
        $sql = "select reg_time, FROM_UNIXTIME(reg_time,'%m-%d') days,count(uid) count from  users where date_sub(CURDATE(), INTERVAL 30 DAY) <= FROM_UNIXTIME(reg_time,'%Y%m%d') group by days";
        $list = $m->query($sql);
        $data = array();

        $total_sql = "select count(*) total from users where reg_time <= ";
        foreach($list as $key => $val){

            $data['day'][$key] = $val['days'];
            $data['count'][$key]= $val['count'];
            $select = $total_sql.$val['reg_time'];
            $totalList = $m->query($select);
            $data['total'][$key] = $totalList[0]['total'];
        }
        $this->ajaxReturn($data);
    }

   public function test(){

       echo 'test';
   }



} 